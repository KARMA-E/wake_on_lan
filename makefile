include global.mk

# Список конфигураций сборки, получаемый из набора файлов, расположенных в папке "build_scripts/configs"
BUILD_CONFIGS := $(basename $(notdir $(wildcard build_scripts/configs/*.mk)))

# Если при вызове make не указана определенная конфигурация, то будет выбрана 
# цель all и собраны все существующие конфигурации из списка BUILD_CONFIGS
all: $(BUILD_CONFIGS)
	@echo "Make ALL done"

# Стирает содержимое папки с артефактами сборки
clean: 
	@echo "Cleaning"
	@$(RMD) $(G_OUTPUT_PATH)

# Шаблон, позволяющий автоматически создать набор целей для каждой конфигурации сборки
# Каждая цель вызывает отдельное выполнение утилиты make с целями сборки, описанными в файле "builder.mk"
%: 
	@echo "----------------------------------------------------------------------------------------------------"
	@echo "Begin to build: $@"
	@make --no-print-directory -f builder.mk -j 8 BC_NAME=$@
