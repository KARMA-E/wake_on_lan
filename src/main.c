#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include <winsock2.h>

#define INVALID_HEX_CHAR            (-1)
#define MAC_ADDR_SIZE               (6U)
#define MAC_PER_MAGIC_PACKET        (16U)

int8_t HexCharToInt(char hex)
{
    if ('0' <= hex && hex <= '9')
    {
        return hex - '0';
    }
    else if ('a' <= hex && hex <= 'f')
    {
        return hex - 'a' + 10;
    }
    else if ('A' <= hex && hex <= 'F')
    {
        return hex - 'A' + 10;
    }
    else
    {
        return INVALID_HEX_CHAR;
    }
}

void MacStr2Int(char const *macStr, uint8_t macOut[MAC_ADDR_SIZE])
{
    const size_t len = strlen(macStr);
    uint8_t octetVal = 0;
    uint8_t octetInd = 0;
    size_t i = 0;
    size_t j = 0;

    while (i < len && octetInd < MAC_ADDR_SIZE)
    {
        int8_t ch = HexCharToInt(macStr[i]);
        if (ch != INVALID_HEX_CHAR)
        {
            octetVal <<= 4;
            octetVal |= ch;
            j++;
            if (j == 2)
            {
                macOut[octetInd] = octetVal;
                octetVal = 0;
                octetInd++;
                j = 0;
            }
        }
        i++;
    }
}

//---------------------------------------------------------------------------------------------------------------------

int main(int argc, char** argv)
{
    uint8_t wolMac[] = {0x1A, 0x2B, 0x3C, 0x4D, 0x5E, 0x6F};

    if (argc == 2)
    {
        MacStr2Int((char*)argv[1], wolMac);
    }
    else if (argc == 1)
    {
        //nop
    }
    else
    {
        printf("Invalid params\n");
        return 1;
    }

    printf("WSA startup\n");
    WSADATA wsaData;
    
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) 
    {
        printf("Winsock boot failed : %d\n", WSAGetLastError());
        return 1;
    }
    
    printf("Create socket\n");
    SOCKET outSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    if (outSocket == INVALID_SOCKET) 
    {
        printf("Failed to create socket : %d\n", WSAGetLastError());
        WSACleanup();
        return 1;
    }

    printf("Enable broadcast mode from socket\n");
    const char enFlag = 1;
    int optStatus = setsockopt(outSocket, SOL_SOCKET, SO_BROADCAST, &enFlag, sizeof(enFlag));

    if (optStatus == SOCKET_ERROR)
    {
        printf("Failed to enable broadcast\n");
    }

#define USE_DEF_ADAPTER         (0U)
#if (USE_DEF_ADAPTER == 1U)
    #warning use def adapter
    SOCKADDR_IN hostAddr;
    hostAddr.sin_family = AF_INET;
    hostAddr.sin_port = htons(12340);
    hostAddr.sin_addr.S_un.S_addr = inet_addr ("192.168.0.11");

    if (bind(outSocket, (PSOCKADDR)&hostAddr, sizeof(hostAddr)))
    {
        printf("Bind FAIL!\n");
        return 1;
    }
#endif

    printf("Generate WoL data pattern\n");
    struct WolData_s {
        uint8_t pref[MAC_ADDR_SIZE];
        uint8_t mac[MAC_PER_MAGIC_PACKET][MAC_ADDR_SIZE];
    } data;
    static_assert(sizeof(data) == 6 * 17, "");

    memset(&(data.pref), 0xFF, sizeof(data.pref));
    for (size_t i = 0; i < MAC_PER_MAGIC_PACKET; i++)
    {
        memcpy(data.mac[i], wolMac, MAC_ADDR_SIZE);
    }

    printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X\n", wolMac[0], wolMac[1], wolMac[2], wolMac[3], wolMac[4], wolMac[5]);
    
    printf("Send packet .. ");
    SOCKADDR_IN wolAddr;
    memset(&wolAddr, 0, sizeof(wolAddr));
    wolAddr.sin_family = AF_INET;
    wolAddr.sin_port = htons(12345);
    wolAddr.sin_addr.S_un.S_addr = inet_addr ("192.168.0.255");
    int status = sendto(outSocket, (char*)&data, sizeof(data), 0, (struct sockaddr *) &wolAddr, sizeof(wolAddr));

    if(status == SOCKET_ERROR) 
    {
        printf("failed %d\n", WSAGetLastError());
    }
    else
    {
        printf("%u bytes transmitted\n", status);
    }

    closesocket(outSocket);
    WSACleanup();

    return 0;
}