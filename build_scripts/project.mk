# Параметры проекта, общие для всех конфигураций сборки

# Выбор компиляции
ifeq ($(OS),Windows_NT)
	GCC     := i686-w64-mingw32-gcc
	G++     := i686-w64-mingw32-g++
	SIZE    := size
	OBJDUMP := objdump
	OBJCOPY := objcopy
else
	GCC     := i686-w64-mingw32-gcc
	G++     := i686-w64-mingw32-g++
	SIZE    := i686-w64-mingw32-size
	OBJDUMP := i686-w64-mingw32-objdump
	OBJCOPY := i686-w64-mingw32-objcopy
endif

# Название проекта
PRJ_NAME := wake_on_lan

# Расширение исполняемого файла (".elf", ".exe", "" и др.)
PRJ_OUT_EXTENSION := .exe

# Флаги компиляции
PRJ_CFLAGS := \
-O2 \

# Флаги линкера
PRJ_LFLAGS := \

# Дефайны
PRJ_DEFINES := \

# Пути к заголовочным файлам
PRJ_INC_PATHS := \

# Пути к файлам с исходным кодом
PRJ_SRC_PATHS := \
src \

# Пути к скомпилированным библиотекам
PRJ_LIB_PATHS := \

# Наименования библиотек
PRJ_LIBRARIES := \
ws2_32 \

PRJ_GCC_DEF := \

# Прочие файлы, при изменении которых необходимо перезапускать сборку
PRJ_OTHER_DEPS := \
