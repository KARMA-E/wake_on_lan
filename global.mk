G_REAL_PATH     := $(realpath .)
G_OUTPUT_PATH   := output

ifeq ($(OS),Windows_NT)
    path=$(subst /,\,$1)
    RM      := busybox rm -rf
    RMD     := busybox rm -rf
    MKD     := busybox mkdir -p
    CP      := busybox cp
else
    path=$(subst \,/,$1)
    RM      := rm -rf
    RMD     := rm -rf
    MKD     := mkdir -p
    CP      := cp
endif
